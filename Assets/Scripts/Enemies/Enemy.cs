using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class Enemy : MonoBehaviour
{
    [SerializeField] protected GameObject player;
    protected PlayerCombat playerCombat;
    protected Rigidbody2D rigidBody;
    [SerializeField] protected Animator animator;
    [SerializeField] float maxHealth;
    [SerializeField] protected float currentHealth;
    [SerializeField] Image healthBarFill;
    [SerializeField] Collider2D aliveCollider;
    [SerializeField] Collider2D deadCollider;
    [SerializeField] Transform wallCheck;
    [SerializeField] LayerMask groundLayers;
    [SerializeField] float attackRange;
    [SerializeField] float maxAgroDistance;
    [SerializeField] float walkSpeed;
    
    
    protected bool isDead;
    protected bool isAttacking;
    protected float playerDistance;
    protected Vector3 playerDirection;
    protected bool isFacingRight = false;
    [SerializeField] float flipPositionOffset;

    [Serializable]
    public struct AttackStance
    {
        public float damage;
        public float animationLockTime;
        public float range;
    }

    private void Awake() {
        if (animator == null)
        {
            animator = GetComponent<Animator>();
        }
        if (player == null)
        {
            player = GameObject.FindWithTag("Player");
        }
        if (wallCheck == null)
        {
            wallCheck = transform.Find("Wall Check");
        }
        if (healthBarFill == null)
        {
            healthBarFill = transform.Find("Canvas").Find("Health Bar").Find("Fill").GetComponent<Image>();
        }
        playerCombat = player.GetComponent<PlayerCombat>();
        rigidBody = GetComponent<Rigidbody2D>();
    }
    // Start is called before the first frame update
    void Start()
    {
        isDead = false;
        isAttacking = false;
        animator.SetBool("b_isDead", false);
        currentHealth = maxHealth;
        UpdateHealthBar(1.0f);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (!isDead && !isAttacking)
        {
            NormalBehavior();
        }
        else if (isDead)
        {
            rigidBody.velocity = Vector2.zero;
        }
    }

    protected virtual void  NormalBehavior()
    {
        playerDirection = player.transform.position - transform.position;
        playerDistance = playerDirection.magnitude;
        if (playerDistance > maxAgroDistance)
        {
            // Idle
            rigidBody.velocity = Vector2.zero;
        }
        else if (playerDistance > attackRange)
        {
            FollowPlayer();
        }
        else
        {
            Attack();
            rigidBody.velocity = Vector2.zero;
        }
        animator.SetFloat("f_speedx", rigidBody.velocity.magnitude);
    }

    protected virtual void FollowPlayer()
    {
        // Flip if facing the wrong direction
        if ((playerDirection.x < 0 && isFacingRight) || (playerDirection.x > 0 && !isFacingRight))
        {
            Flip();
        }

        Vector2 newVelocity = new Vector2(0, rigidBody.velocity.y);
        // Check if is facing a wall
        Collider2D[] colliders = Physics2D.OverlapCircleAll(wallCheck.position, 0.5f, groundLayers);
		if (colliders.Length == 0)
        {
            Vector3 walkVelocity = playerDirection.normalized * walkSpeed;
            newVelocity = new Vector2(walkVelocity.x, rigidBody.velocity.y);
        }

        rigidBody.velocity = newVelocity;
    }

    protected IEnumerator AnimationLock(float animationLockTime)
    {
        isAttacking = true;

        yield return new WaitForSeconds(animationLockTime);

        isAttacking = false;
    }

    public virtual void TakeDamage(float damage)
    {
        if (isDead)
            return;
        
        currentHealth -= damage;
        animator.SetTrigger("t_takeHit");
        if (currentHealth <= 0)
        {
            currentHealth = 0;
            UpdateHealthBar(0.0f);
            StartCoroutine(Die());
        }
        else
        {
            float fillAmount = currentHealth / maxHealth;
            UpdateHealthBar(fillAmount);
        }
    }

    public void UpdateHealthBar(float fillAmount)
    {
        healthBarFill.fillAmount = fillAmount;
        if (fillAmount == 1.0f)
        {
            healthBarFill.transform.parent.gameObject.SetActive(false);
        }
        else if (!healthBarFill.transform.parent.gameObject.activeInHierarchy)
        {
            healthBarFill.transform.parent.gameObject.SetActive(true);
        }
    }

    public IEnumerator Die()
    {
        isDead = true;
        animator.SetBool("b_isDead", true);
        aliveCollider.enabled = false;
        //deadCollider.enabled = true;
        rigidBody.isKinematic = true;

        yield return new WaitForSeconds(2);

        Destroy(gameObject);
    }

    private void Flip()
	{
		if (isFacingRight)
		{
			transform.position += Vector3.left * flipPositionOffset;
		}
		else
		{
			transform.position += Vector3.right * flipPositionOffset;
		}
		// Switch the way the player is labelled as facing.
		isFacingRight = !isFacingRight;

		// Multiply the player's x local scale by -1.
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}

    public abstract void HitDetection(int stanceIndex);

    protected abstract void Attack();
}
