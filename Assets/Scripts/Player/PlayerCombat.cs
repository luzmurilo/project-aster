using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerCombat : MonoBehaviour
{
    private CharacterController2D controller;
    [SerializeField] Animator animator;
    [SerializeField] float maxHealth;
    public float currentHealth;// {get; private set;}
    [SerializeField] Transform attackPoint;
    private int currentStance;
    public bool inputReady {get; private set;}
    public bool isInvulnerable;
    private string inputInQueue;
    
    [Serializable]
    public struct AttackStance
    {
        public float damage;
        public float animationLockTime;
    }
    [SerializeField] List<AttackStance> stances;
    [SerializeField] AttackStance dashStance;
    [SerializeField] LayerMask enemyLayers;

    private void Awake() 
    {
        controller = GetComponent<CharacterController2D>();
        CharacterController2D.playerInput.FindAction("Attack").performed += Attack;

        if (animator == null)
        {
            animator = GetComponent<Animator>();
        }
    }

    private void Start()
    {
        currentStance = 0;
        inputReady = true;
        isInvulnerable = false;
        currentHealth = maxHealth;
        InGameUI.SharedInstance.UpdateHealthBar(1.0f);
    }

    // Update is called once per frame
    void Update()
    {
        // Calling saved input if one exists.
        if (inputInQueue != null && !controller.isDead && !controller.isAttacking)
        {
            Invoke(inputInQueue, 0f);
            inputInQueue = null;
        }
    }

    void Attack(InputAction.CallbackContext context)
    {
        if (!inputReady || controller.m_wasCrouching || controller.isDead || !controller.m_Grounded)
            return;

        // If the player press 'Attack' after the half-point of the current attack, the input will be saved.
        if (controller.isAttacking)
        {
            inputInQueue = "NormalAttack";
            return;
        }
        
        if (controller.isRunning || controller.isDashing)
        {
            DashAttack();
        }
        else
        {
            NormalAttack();
        }

        //currentStance ++;

        // if (currentStance >= stances.Count)
        //     currentStance = 0;

    }

    void NormalAttack()
    {
        controller.Move(0.0f, false, false, false);
        animator.SetTrigger("t_attack");
        StartCoroutine(AnimationLock(stances[currentStance].animationLockTime));
    }

    void DashAttack()
    {
        animator.SetTrigger("t_dashAttack");
        StartCoroutine(AnimationLock(dashStance.animationLockTime));
    }

    public void HitDetection(float range)
    {
        if (attackPoint != null)
        {
            Collider2D[] enemiesHit = Physics2D.OverlapCircleAll(attackPoint.position, range, enemyLayers);

            foreach (Collider2D enemy in enemiesHit)
            {
                enemy.GetComponent<Enemy>().TakeDamage(stances[currentStance].damage);
            }
        }
        else
        {
            Debug.Log("No attack point defined!");
        }
    }

    IEnumerator AnimationLock(float animationLockTime)
    {
        controller.isAttacking = true;
        inputReady = false;

        yield return new WaitForSeconds(animationLockTime / 2);

        inputReady = true;      // If the player press 'Attack' after the half-point of the current attack, the input will be saved. 

        yield return new WaitForSeconds(animationLockTime / 2);

        controller.isAttacking = false;
    }

    public void SetAttackStance(int newStance)
    {
        currentStance = newStance;
        animator.SetInteger("i_attackStance", currentStance);
    }

    public void TakeDamage(float damage, bool forceAnimation)
    {
        if (controller.isDead || isInvulnerable)
        {
            return;
        }
        
        MakeInvulnerable(1.0f);

        currentHealth -= damage;

        if (forceAnimation)
        {
            animator.SetTrigger("t_takeHit");
            controller.SetVelocity(0, 0);
        }

        if (currentHealth <= 0)
        {
            Die();
        }
        else
        {
            float fillAmount = currentHealth / maxHealth;
            InGameUI.SharedInstance.UpdateHealthBar(fillAmount);
        }
    }

    public void Die()
    {
        currentHealth = 0;
        InGameUI.SharedInstance.UpdateHealthBar(0.0f);

        if(!controller.isDead)
            controller.Die();
        
        animator.SetBool("b_isDead", true);
    }

    public void MakeInvulnerable(float seconds)
    {
        StartCoroutine(InvulnerabilityTimer(seconds));
    }

    IEnumerator InvulnerabilityTimer(float seconds)
    {
        isInvulnerable = true;
        yield return new WaitForSeconds(seconds);
        isInvulnerable = false;
    }


    private void OnDrawGizmosSelected()
    {
        if (attackPoint == null)
            return;
        
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(attackPoint.position, 0.5f);
        
    }
}
