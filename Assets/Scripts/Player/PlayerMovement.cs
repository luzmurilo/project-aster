using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerMovement : MonoBehaviour
{

    private CharacterController2D controller;
    private Rigidbody2D playerRB;
    [SerializeField] private Animator animator;
    private float horizontalInput = 0f;
    private bool jump = false;
    private bool dash = false;
    private bool crouch = false;
    public float walkSpeed = 4.0f;

    private void Awake() 
    {
        if (animator == null)
            animator = GetComponent<Animator>();
        playerRB = GetComponent<Rigidbody2D>();
        controller = GetComponent<CharacterController2D>();
        CharacterController2D.playerInput.FindAction("Jump").performed += Jump;
        CharacterController2D.playerInput.FindAction("Dash").performed += Dash;
    }

    private void Update() 
    {
        horizontalInput = CharacterController2D.playerInput.FindAction("Movement").ReadValue<float>();
        crouch = CharacterController2D.playerInput.FindAction("Crouch").IsPressed();

        float moveSpeed = horizontalInput * walkSpeed;

        controller.Move(moveSpeed, crouch, jump, dash);

        jump = false;
        dash = false;

        if ((Mathf.Abs(playerRB.velocity.x) >= walkSpeed * 0.95f) && !(controller.isSliding || !controller.m_Grounded))
        {
            controller.isRunning = true;
        }
        else
        {
            controller.isRunning = false;
        }
    }

    private void Jump(InputAction.CallbackContext context)
    {
        jump = true;
    }

    private void Dash(InputAction.CallbackContext context)
    {
        dash = true;
    }
}
