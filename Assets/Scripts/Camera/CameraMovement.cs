using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    [SerializeField] GameObject player;
    public Vector2 offset;
    public bool followPlayer = true;
    private float cameraSpeed;
    [SerializeField] float cameraMaxSpeed;
    [SerializeField] float cameraMinSpeed;
    [SerializeField] float cameraMaxYPos;
    private Rigidbody2D cameraRB;
    private CharacterController2D playerController;
    private Vector2 target;

    private void Awake() 
    {
        cameraRB = GetComponent<Rigidbody2D>();
        if (player == null)
        {
            player = GameObject.FindWithTag("Player");
        }
        if (player != null)
        {
            playerController = player.GetComponent<CharacterController2D>();
        }
        else
        {
            Debug.Log("Player Object not found!");
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        transform.position = player.transform.position + new Vector3(offset.x, offset.y, transform.position.z);
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (followPlayer)
        {
            target = new Vector2(player.transform.position.x + (offset.x * playerController.directionFacing), player.transform.position.y + offset.y);
        }
        if (target != null)
        {
            FollowTarget(target);
        }
    }

    void FollowTarget(Vector2 target)
    {
        Vector2 direction = new Vector2(target.x - transform.position.x, target.y - transform.position.y);

        if (direction.magnitude > 0.2f)
        {
            cameraSpeed = Mathf.Min(cameraMaxSpeed, direction.magnitude * 2f);
            cameraSpeed = Mathf.Max(cameraMinSpeed, cameraSpeed);
            float speedMultiplierY = 2;
            if (transform.position.y >= cameraMaxYPos && direction.y > 0)
            {
                speedMultiplierY = 0;
                transform.position = new Vector3(transform.position.x, cameraMaxYPos, transform.position.z);
            }
            cameraRB.velocity = direction.normalized * new Vector2(cameraSpeed, cameraSpeed * speedMultiplierY);
        }
        else
        {
            cameraRB.velocity = Vector2.zero;
        }
    }

    public void StopMovement()
    {
        followPlayer = false;
    }
}
