using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxBackground : MonoBehaviour
{
    [SerializeField] bool infinityHorizontal;
    [SerializeField] bool infinityVertical;
    [SerializeField] Transform cameraTransform;
    private Vector3 lastCameraPosition;

    [SerializeField] Vector2 parallaxMultiplier;

    private float textureUnitSizeX;
    private float textureUnitSizeY;

    private void Awake() {
        if (cameraTransform == null)
            cameraTransform = GameObject.Find("Main Camera").GetComponent<Transform>();
    }

    // Start is called before the first frame update
    void Start()
    {
        lastCameraPosition = cameraTransform.position;
        Sprite sprite = GetComponent<SpriteRenderer>().sprite;
        Texture2D texture = sprite.texture;
        textureUnitSizeX = texture.width / sprite.pixelsPerUnit;
        textureUnitSizeY = texture.height / sprite.pixelsPerUnit;
        transform.position = new Vector3(cameraTransform.position.x, transform.position.y, transform.position.z);
    }

    // Update is called once per frame
    void LateUpdate()
    {
        Vector3 deltaMovement = cameraTransform.position - lastCameraPosition;
        transform.position += new Vector3(deltaMovement.x * parallaxMultiplier.x, deltaMovement.y * parallaxMultiplier.y, transform.position.z);
        lastCameraPosition = cameraTransform.position;

        if (infinityHorizontal)
        {
            if (Mathf.Abs(cameraTransform.position.x - transform.position.x) >= textureUnitSizeX)
            {
                float offsetx = (cameraTransform.position.x - transform.position.x) % textureUnitSizeX;
                transform.position = new Vector3(cameraTransform.position.x - offsetx, transform.position.y, transform.position.z);
            }
        }
        if (infinityVertical)
        {
            if (Mathf.Abs(cameraTransform.position.y - transform.position.y) >= textureUnitSizeY)
            {
                float offsety = (cameraTransform.position.y - transform.position.y) % textureUnitSizeY;
                transform.position = new Vector3(transform.position.x, cameraTransform.position.y + offsety, transform.position.z);
            }
        }
    }
}
